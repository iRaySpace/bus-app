import BusStore from "../store/DomainStore/BusStore";
import MainStore from "../store/DomainStore/HomeStore";
import TicketStore from "../store/DomainStore/TicketStore";
import AnnouncementStore from "../store/DomainStore/AnnouncementStore";

import LoginStore from "../store/ViewStore/LoginViewStore";
import CreateTicket from "../store/ViewStore/CreateTicketViewStore";
import RegistrationStore from "../store/ViewStore/RegistrationViewStore";
import Seats from "../store/ViewStore/SeatsViewStore";

export default function() {
	const busStore = new BusStore();
	const mainStore = new MainStore();
	const ticketStore = new TicketStore();
	const newsStore = new AnnouncementStore();
	
	const loginForm = new LoginStore();
	const registrationForm = new RegistrationStore();
	const ticketForm = new CreateTicket();
	const seatForm = new Seats();

	return {
		registrationForm,
		ticketForm,
		seatForm,
		loginForm,
		busStore,
		mainStore,
		newsStore,
		ticketStore
	};
}
