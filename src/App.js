import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";

import Login from "./container/LoginContainer";
import Home from "./container/HomeContainer";
import BlankPage from "./container/BlankPageContainer";
import Sidebar from "./container/SidebarContainer";
import BookSeat from "./container/BookSeatContainer";
import BusSchedule from "./container/BusScheduleContainer";
import FrontDesk from "./container/FrontDeskContainer";
import Registration from "./container/RegistrationContainer";
import AvailableBus from "./container/AvailableBusContainer";
import PaymentDetails from "./container/PaymentDetailsContainer";
import TicketDetails from "./container/TicketDetailsContainer";
import Announcements from "./container/AnnouncementsContainer";
import PaymentSummary from "./container/PaymentSummaryContainer";
import CreateTicket from "./container/CreateTicketContainer";
import TicketSummary from "./container/TicketSummaryContainer";
import Tickets from "./container/TicketsContainer";
import Seats from "./container/SeatsContainer";
import ReserveSuccess from "./container/ReserveSuccessContainer";
import TrackBus from "./container/TrackBusContainer";

const Drawer = DrawerNavigator(
	{
		Home: { screen: Home },
		Seats: { screen: Seats },
		BookSeat: { screen: BookSeat },
		FrontDesk: { screen: FrontDesk },
		BusSchedule: { screen: BusSchedule },
		AvailableBus: { screen: AvailableBus },
		CreateTicket: { screen: CreateTicket },
		TicketDetails: { screen: TicketDetails },
		Announcements: { screen: Announcements },
		PaymentDetails: { screen: PaymentDetails },
		PaymentSummary: { screen: PaymentSummary },
		TicketSummary: { screen: TicketSummary },
		Tickets: { screen: Tickets },
		ReserveSuccess: { screen: ReserveSuccess },
		TrackBus: { screen: TrackBus }
	},
	{
		initialRouteName: "FrontDesk",
		contentComponent: props => <Sidebar {...props} />,
	}
);

const App = StackNavigator(
	{
		Login: { screen: Login },
		Registration: { screen: Registration },
		BlankPage: { screen: BlankPage },
		Drawer: { screen: Drawer },
	},
	{
		initialRouteName: "Login",
		headerMode: "none",
	}
);

export default () => (
	<Root>
		<App />
	</Root>
);
