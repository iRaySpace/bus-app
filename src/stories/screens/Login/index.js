import * as React from "react";
import { Container, Button, Text, View } from "native-base";
import { Image, TouchableOpacity } from "react-native";

class Login extends React.Component {
	render() {
		return (
			 <Container>
					<View style={{ paddingLeft: 15, paddingRight: 15, paddingTop: 50 }}>
						<Image
							source={{uri: "main_logo"}}
							style={{
								width: 150,
								height: 180,
								alignSelf: "center"
							}}
						/>
						<Text style={{ textAlign: "center", fontSize: 24, marginBottom: 24, color: "#aaa" }}>
							Login Information
						</Text>
						{this.props.loginForm}
						<View padder>
							<Button full block onPress={() => this.props.onLogin()}>
								<Text>Login</Text>
							</Button>
							<Button full block info style={{ marginTop: 10 }} onPress={() => this.props.onRegister()}>
								<Text>Register Account</Text>
							</Button>
						</View>
		      </View>
		    </Container>
		);
	}
}

export default Login;
