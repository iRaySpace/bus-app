import * as React from "react";
import { TouchableOpacity, View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Text
} from "native-base";
import { Table, TableWrapper, Cell } from "react-native-table-component";

class Seats extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: [
        ["A1", "A2", "", "A3", "A4"],
        ["B1", "B2", "", "B3", "B4"],
        ["C1", "C2", "", "C3", "C4"],
        ["D1", "D2", "", "D3", "D4"],
        ["E1", "E2", "", "E3", "E4"],
        ["F1", "F2", "", "F3", "F4"],
        ["G1", "G2", "", "G3", "G4"],
        ["H1", "H2", "", "H3", "H4"],
        ["I1", "I2", "", "I3", "I4"],
        ["J1", "J2", "", "J3", "J4"]
      ]
    };
  }

  render() {

    const Reserve = (data) => {
      
      // Grey
      let color = "#ccc";

      // If reserved, set as `forest` color
      if (this.props.isSeatReserved(data)) {
        color = "#64dd17";
      }

      // If taken, set as `blue` color
      if (this.props.isSeatTaken(data)) {
        color = "#3f51b5";
      }

      return (
        <TouchableOpacity onPress={() => this.props.onReserve(data)}>
          <View style={{ backgroundColor: color, padding: 5 }}>
            <Text style={{ textAlign: "center" }}>{data}</Text>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Choose Seats</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon
                active
                name="arrow-forward"
                onPress={() => this.props.navigation.navigate("TicketSummary")}
              />
            </Button>
          </Right>
        </Header>
        <Content padder>
          <Table borderStyle={{ borderColor: "transparent" }}>
            {
              this.state.tableData.map((rowData, index) => (
                <TableWrapper key={index} style={{ flexDirection: "row", height: 40 }}>
                  {
                    rowData.map((cellData, cellIndex) => (
                      <Cell key={`${index}${cellIndex}`} data={cellIndex === 2 ? "" : Reserve(cellData)} />
                    ))
                  }
                </TableWrapper>
              ))
            }
          </Table>
        </Content>
      </Container>
    );
  }
}

export default Seats;
