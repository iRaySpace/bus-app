import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Picker
} from "native-base";

import { View, Text } from "react-native";

class PaymentDetails extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      quantityValue: "1"
    };
  }
  
  render() {
    const { bus } = this.props;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Payment Details</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem header style={{ paddingBottom: 0 }}>
              <Text style={{ fontWeight: "bold", fontSize: 36 }}>
                {bus.departure_time}
              </Text>
              <Text style={{ fontWeight: "bold", marginLeft: 5, color: "#aaa" }}>
                Departure Time
              </Text>
            </CardItem>
            <CardItem>
              <Body>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontWeight: "bold", color: "#333" }}>
                    {bus.from}
                  </Text>
                  <Text>—</Text>
                  <Text style={{ fontWeight: "bold", color: "#333" }}>
                    {bus.to}
                  </Text>
                </View>
                <Text>
                  {bus.bus_liner}/{bus.bus_id}
                </Text>
              </Body>
            </CardItem>
          </Card>
          <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <Text>Quantity</Text>
            <Picker
              note
              mode="dropdown"
              style={{ width: 120 }}
              selectedValue={this.state.quantityValue}
              onValueChange={(e) => this.setState({ quantityValue: e })}
            >
              <Picker.Item label="1" value="1" />
              <Picker.Item label="2" value="2" />
              <Picker.Item label="3" value="3" />
              <Picker.Item label="4" value="4" />
              <Picker.Item label="5" value="5" />
            </Picker>
          </View>
          <Button full onPress={() => {
            this.props.onBook(this.state.quantityValue);
          }}>
            <Text style={{ color: "white" }}>Book</Text>
          </Button>
        </Content>
      </Container>
    );
  }

}

export default PaymentDetails;
