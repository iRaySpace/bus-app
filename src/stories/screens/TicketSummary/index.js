import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Card,
  CardItem
} from "native-base";

class TicketSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { payment, seats } = this.props;

    const Seats = seats.map((seat, index) => (
      <Text key={index}>[{seat}]</Text>
    ));

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Ticket</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem style={{ paddingBottom: 0 }}>
              <Text style={{ fontWeight: "bold", fontSize: 24 }}>
                {payment.customer}
              </Text>
            </CardItem>
            <CardItem style={{ paddingTop: 0 }}>
              <Text style={{ color: "#a9a9a9" }}>
                Payment Method: {payment.method}
              </Text>
            </CardItem>
            <CardItem style={{ paddingBottom: 0 }}>
              <Text style={{ fontWeight: "bold", fontSize: 21 }}>
                {payment.busRoute}
              </Text>
            </CardItem>
            <CardItem style={{ paddingTop: 0 }}>
              <Text style={{ color: "#a9a9a9" }}>
                {payment.from}/{payment.to}
              </Text>
            </CardItem>
            <CardItem style={{ paddingBottom: 0 }}>
              <Text style={{ fontWeight: "bold" }}>
                Fare: {payment.fare} x {payment.pax} pax
              </Text>
            </CardItem>
            <CardItem>
              <Text style={{ fontWeight: "bold" }}>
                Payment Total: {payment.paid}
              </Text>
            </CardItem>
            <CardItem style={{ borderTopWidth: 1, borderColor: "#e7e7e7" }}>
              <Text>Reserve Seats: </Text>
              {Seats}
            </CardItem>
          </Card>
          <Button
            full
            onPress={() => this.props.onTicket()}
          >
            <Text>
              Ticket
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default TicketSummary;
