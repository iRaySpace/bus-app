import * as React from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Text
} from "native-base";

class AvailableBus extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    // ''' Submit Payment details '''
    const BusComponent = this.props.buses.map((bus, i) =>
      <Card key={i}>
        <CardItem header style={{ paddingBottom: 0 }}>
          <Text style={{ fontWeight: "bold", fontSize: 36 }}>
            {bus.departure_time}
          </Text>
          <Text style={{ fontWeight: "bold", marginLeft: 5, color: "#aaa" }}>
            Departure Time
          </Text>
        </CardItem>
        <CardItem>
          <Body>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ fontWeight: "bold", color: "#333" }}>
                {bus.from}
              </Text>
              <Text>—</Text>
              <Text style={{ fontWeight: "bold", color: "#333" }}>
                {bus.to}
              </Text>
            </View>
            <Text>
              {bus.bus_liner}/{bus.bus_id}
            </Text>
          </Body>
        </CardItem>
        <CardItem footer button onPress={() => this.props.onSelectBus(i) }>
          <Icon name="arrow-forward" style={{ marginRight: 5, color: "#7AC70D" }} />
          <Text style={{ fontWeight: "bold", marginLeft: 5, color: "#7AC70D" }}>
            Book Now at PHP {bus.fare}
          </Text>
        </CardItem>
      </Card>
    );

    const Buses = (this.props.buses.length > 0)
      ? BusComponent
      : <Text>No buses scheduled.</Text>;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Available Bus</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          {Buses}
        </Content>
      </Container>
    );
  }
}

export default AvailableBus;
