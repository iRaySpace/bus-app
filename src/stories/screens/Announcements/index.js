import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Text
} from "native-base";

class Announcements extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    // '''Render application layout for the news'''
    const NewsComponent = this.props.news.map((news, id) =>
      <Card key={id}>
        <CardItem header>
          <Text style={{ fontWeight: "bold", fontSize: 18 }}>
            {news.title}
          </Text>
        </CardItem>
        <CardItem>
          <Text>{news.content}</Text>
        </CardItem>
        <CardItem footer>
          <Text style={{ color: "#aaa" }}>
            Posted on: {news.posted}
          </Text>
        </CardItem>
      </Card>
    );

    const NewsOrNot = (NewsComponent.length > 0)
      ? NewsComponent
      : <Text>There are no news.</Text>;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Announcements</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          {NewsOrNot}
        </Content>
      </Container>
    );
  }
}

export default Announcements;
