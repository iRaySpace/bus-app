import * as React from "react";
import { Alert } from "react-native"; 
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Text,
  Card,
  CardItem
} from "native-base";

class Tickets extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    const Tickets = this.props.tickets.map((ticket, index) => (
      <Card key={index}>
        <CardItem header style={{ backgroundColor: "#3f51b5", borderRadius: 0, justifyContent: "space-between" }}>
          <Text style={{ fontWeight: "bold", fontSize: 24, color: "white" }}>
            {ticket.ticket_number}
          </Text>
          <Button
            danger
            onPress={() => {
              // Prompt if wanna cancel
              Alert.alert(
                "Cancel Ticket",
                `Would you like to cancel ticket ${ticket.ticket_number}?`,
                [
                  {text: "No", style: "cancel"},
                  {text: "Yes", onPress: () => this.props.onCancel(index)}
                ]
              );
            }}>
            <Icon name="close" />
          </Button>
        </CardItem>
        <CardItem>
          <Text style={{ fontWeight: "bold", fontSize: 18, color: "#a9a9a9" }}>
            Seat Number: {ticket.seat_number}
          </Text>
        </CardItem>
        <CardItem style={{ paddingBottom: 0 }}>
          <Text style={{ fontWeight: "bold" }}>
            Fare: P{ticket.ticket_price}
          </Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0, borderBottomWidth: 1, borderColor: "#e7e7e7" }}>
          <Text style={{ fontWeight: "bold" }}>
            Departure: {ticket.departure_time}/{ticket.departure_date}
          </Text>
        </CardItem>
        <CardItem style={{ paddingBottom: 0 }}>
          <Text style={{ fontWeight: "bold" }}>
            {ticket.bus_id}
          </Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0, paddingBottom: 0 }}>
          <Text>{ticket.bus_liner}</Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0 }}>
          <Text>{ticket.destination_route}</Text>
        </CardItem>
      </Card>
    ));

    const TicketsOrNo = (Tickets.length > 0)
      ? Tickets
      : <Text>There are no tickets.</Text>;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Tickets</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          {TicketsOrNo}
        </Content>
      </Container>
    );
  }
}

export default Tickets;
