import * as React from "react";
import { Image } from "react-native";
import { Container, Button, Text, View, Content } from "native-base";

class Registration extends React.Component {
	render() {
		return (
			<Container>
				<Content padder style={{ paddingTop: 30 }}>
					<Image
						source={{uri: "main_logo"}}
						style={{
							width: 150,
							height: 180,
							alignSelf: "center"
						}}
					/>
					<Text style={{ textAlign: "center", fontSize: 24, color: "#aaa", marginBottom: 15 }}>
						Account Registration
					</Text>
					{this.props.registrationForm}
					<View padder>
						<Button full block success onPress={() => this.props.onRegistration()}>
							<Text>Register</Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}

export default Registration;
