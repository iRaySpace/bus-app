import * as React from "react";
import { 
	  Container,
	  Header,
	  Title,
	  Content,
	  Text,
	  Button,
	  Icon,
	  Left,
	  Body,
	  Right,
	  List,
	  ListItem,
	  Card,
	  CardItem } from "native-base";
import { TextInput, View } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

import styles from "./styles";
export interface Props {
  navigation: any;
  list: any;
}
export interface State {}

class BusSchedule extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Bus Schedule</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
        <Container>
        	<Card>
	        	<CardItem header bordered>
	        		<Row>
						<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 16}} center>March 24, 2018</Text>
					</Row>
	        		<Row>
						<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 16}} center>Cagayan de Oro to Dipolog</Text>
					</Row>
	            </CardItem>
				<CardItem bordered>
				<Grid>
					<Row style={{ padding: 30}}>
						<Text style={{ justifyContent: "center", textAlign: "center", fontSize: 20, fontWeight: 'bold'}} center>Non-Stop Airconditioned Bus</Text>
					</Row>
					<Row>
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>9:30 pm</Text>
						</Col>
						<Col>
							<Text style={{ textAlign: "center", fontSize: 20}}> to </Text>
						</Col>	
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>8:00 am</Text>
						</Col>					
					</Row>
					<Row style={{ padding: 30}}>
						<Text style={{ justifyContent: "center", textAlign: "center", fontSize: 20, fontWeight: 'bold'}} center>₱ 341</Text>
					</Row>
				</Grid>						
	            </CardItem>
	            <CardItem bordered>
				<Grid>
					<Row>
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>9:30 pm</Text>
						</Col>
						<Col>
							<Text style={{ textAlign: "center", fontSize: 20}}> to </Text>
						</Col>	
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>8:00 am</Text>
						</Col>					
					</Row>
					<Row style={{ padding: 30}}>
						<Text style={{ justifyContent: "center", textAlign: "center", fontSize: 20, fontWeight: 'bold'}} center>₱ 341</Text>
					</Row>
				</Grid>						
	            </CardItem>
	            <CardItem bordered>
				<Grid>
					<Row>
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>9:30 pm</Text>
						</Col>
						<Col>
							<Text style={{ textAlign: "center", fontSize: 20}}> to </Text>
						</Col>	
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>8:00 am</Text>
						</Col>					
					</Row>
					<Row style={{ padding: 30}}>
						<Text style={{ justifyContent: "center", textAlign: "center", fontSize: 20, fontWeight: 'bold'}} center>₱ 341</Text>
					</Row>
				</Grid>						
	            </CardItem>
	            <CardItem bordered>
				<Grid>
					<Row>
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>9:30 pm</Text>
						</Col>
						<Col>
							<Text style={{ textAlign: "center", fontSize: 20}}> to </Text>
						</Col>	
						<Col>
							<Text style={{ textAlign: "center", fontWeight: 'bold', fontSize: 20}}>8:00 am</Text>
						</Col>					
					</Row>
					<Row style={{ padding: 30}}>
						<Text style={{ justifyContent: "center", textAlign: "center", fontSize: 20, fontWeight: 'bold'}}>₱ 341</Text>
					</Row>
				</Grid>						
	            </CardItem>
			        <Button  full>
		             <Text>Go!</Text>
		            </Button>
	          </Card>
        </Container>
        	
        </Content>
    </Container>
      
    );
  }
}

export default BusSchedule;