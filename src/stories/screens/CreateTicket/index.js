import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Text
} from "native-base";
import QRCodeScanner from "react-native-qrcode-scanner";

class CreateTicket extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    // Get the data
    const { scanned, payment } = this.props;

    // Payment Component
    const PaymentComponent = (
      <Card>
        <CardItem style={{ paddingBottom: 0 }}>
          <Text style={{ fontWeight: "bold", fontSize: 24 }}>
            {payment.customer}
          </Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0 }}>
          <Text style={{ color: "#a9a9a9" }}>
            Payment Method: {payment.method}
          </Text>
        </CardItem>
        <CardItem style={{ paddingBottom: 0 }}>
          <Text style={{ fontWeight: "bold", fontSize: 21 }}>
            {payment.busRoute}
          </Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0 }}>
          <Text style={{ color: "#a9a9a9" }}>
            {payment.from}/{payment.to}
          </Text>
        </CardItem>
        <CardItem>
          <Text style={{ fontWeight: "bold" }}>
            Fare: {payment.fare} x {payment.pax} pax
          </Text>
        </CardItem>
        <CardItem>
          <Text style={{ fontWeight: "bold" }}>
            Payment Total: {payment.paid}
          </Text>
        </CardItem>
      </Card>
    );

    // Should display if wala pa ka scan 
    const QRComponent = (!scanned)
      ? <QRCodeScanner onRead={(e) => this.props.onRead(e)} />
      : PaymentComponent;

    // Scan again button
    const ScanComponent = (scanned)
      ? (
          <Button
            full 
            block
            style={{ marginTop: 15 }}
            onPress={() => this.props.onScan()}>
            <Text>Scan Again</Text>
          </Button>
        )
      : null;

    const SeatComponent = (scanned)
        ? (
          <Button
            full
            block
            style={{ marginTop: 15 }}
            onPress={() => this.props.navigation.navigate("Seats")}>
            <Text>Select Seats</Text>
          </Button>
          )
        : null;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Create Ticket</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          {QRComponent}
          {ScanComponent}
          {SeatComponent}
        </Content>
      </Container>
    );
  }
}

export default CreateTicket;
