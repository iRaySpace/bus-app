import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Text
} from "native-base";

class ReserveSuccess extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Success</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ paddingTop: 45 }}>
          <Icon active name="checkmark" style={{ fontSize: 64, alignSelf: "center", color: "#7AC70D" }}/>
          <Text style={{ textAlign: "center", fontSize: 36, fontWeight: "bold", color: "#a9a9a9" }}>
            Ticket reserved
          </Text>
          <Button
            full
            block
            style={{ marginTop: 30 }}
            onPress={() => this.props.navigation.navigate("CreateTicket")}>
            <Text>
              Create Ticket
            </Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

export default ReserveSuccess;
