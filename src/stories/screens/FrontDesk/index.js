import * as React from "react";
import {
	Container,
	Header,
	Title,
	Icon,
	Left,
	Button,
	Body,
	Right
} from "native-base";
import { View, Text } from "react-native";

import styles from "./styles";

class FrontDesk extends React.Component {
	constructor(props) {
    super(props);
  }

  render() {
		// ''' Render the buttons navigating to other options '''

		const store = this.props.mainStore;

		// Get the full name and balance
		const fullName = store.firstName + " " + store.lastName;
		const balance = store.balance;

    return (
      <Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent>
							<Icon
								active
								name="menu"
								onPress={() => this.props.navigation.navigate("DrawerOpen")}
							/>
						</Button>
					</Left>
					<Body>
						<Title>Front Desk</Title>
					</Body>
					<Right />
				</Header>
        <View style={{ paddingLeft: 15, paddingRight: 15 }}>
	        <View padder>
	        	<Text style={{
							textAlign: "center",
							fontWeight: "bold",
							fontSize: 18,
							marginTop: 15
						}}>
							Hello {fullName}!
						</Text>
						<Text style={{
							textAlign: "center",
							fontWeight: "bold",
							fontSize: 18,
							marginTop: 5
						}}>
							Your Buszer balance is {balance}.
						</Text>
						<Text style={{
							textAlign: "center",
							fontWeight: "bold",
							fontSize: 24,
							marginTop: 15,
							padding: 15
						}}>
							Would you like to?
						</Text>
	        </View>
					<View style={{ marginBottom: 15 }}>
						<Button
							full
							onPress={() => this.props.navigation.navigate("BookSeat")}
							style={{
								backgroundColor: "#7AC70D"
						}}>
							<Text style={{
								color: "white",
								fontSize: 21,
								fontWeight: "bold"
							}}>
								Book a seat
							</Text>
						</Button>
					</View>
					<View style={{ marginBottom: 15 }}>
						<Button
							full
							onPress={() => this.props.navigation.navigate("BusSchedule")}
							style={{
								backgroundColor: "#1CB0F6",
						}}>
							<Text style={{
								color: "white",
								fontSize: 21,
								fontWeight: "bold"
							}}>
								View schedule
							</Text>
						</Button>
					</View>
					<View style={{ marginBottom: 15 }}>
						<Button
							full
							onPress={() => this.props.navigation.navigate("Announcements")}
						>
							<Text style={{
								color: "white",
								fontSize: 21,
								fontWeight: "bold"
							}}>
								View Announcements
							</Text>
						</Button>
					</View>
					<View style={{ marginBottom: 15 }}>
						<Button
							full
							onPress={() => this.props.navigation.navigate("Tickets")}
						>
							<Text style={{
								color: "white",
								fontSize: 21,
								fontWeight: "bold"
							}}>
								View Tickets
							</Text>
						</Button>
					</View>
					<View>
						<Button
							full
							onPress={() => this.props.navigation.navigate("TrackBus")}
						>
							<Text style={{
								color: "white",
								fontSize: 21,
								fontWeight: "bold"
							}}>
								Track Bus
							</Text>
						</Button>
					</View>
      	</View>
    </Container>
    );
  }
}

export default FrontDesk;
