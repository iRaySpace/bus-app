import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  Text
} from "native-base";
import QRCode from "react-native-qrcode";

class PaymentSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Payment</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card style={{ alignItems: "center", paddingTop: 15, paddingBottom: 15 }}>
            <QRCode
              value={this.props.code}
              size={200}
              bgColor="#000080"
              fgColor="white"
            />
            <Text style={{ textAlign: "center", fontWeight: "bold", fontSize: 24, paddingTop: 15 }}>Present QR Code to Conductor</Text>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default PaymentSummary;
