import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem
} from "native-base";

import { Text } from "react-native";

class TicketDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Ticket Details</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem header style={{ justifyContent: "space-between" }}>
              <Text style={{ color: "#ffb020", fontWeight: "bold", fontSize: 18 }}>
                Pabama Transport
              </Text>
              <Text style={{ color: "#2196f3" }}>
                17:30
              </Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Transaction ID#</Text>
              <Text>0asfd</Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Quantity</Text>
              <Text>4</Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Rate</Text>
              <Text>65.00</Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Paid Amount</Text>
              <Text>260.00</Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Paid via</Text>
              <Text>Buszer Points</Text>
            </CardItem>
            <CardItem style={{ justifyContent: "space-between" }}>
              <Text style={{ fontWeight: "bold" }}>Date</Text>
              <Text>10 August 2018 - 17:15</Text>
            </CardItem>
          </Card>
          <Button
            full
            onPress={() => this.props.navigation.navigate("FrontDesk")}
            style={{ backgroundColor: "white" }}
          >
            <Text style={{ color: "#ffb020" }}>
              Book
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default TicketDetails;
