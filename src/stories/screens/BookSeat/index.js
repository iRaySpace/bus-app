import * as React from "react";
import { 
	Container,
	Header,
	Title,
	Content,
	Button,
	Icon,
	Left,
	Body,
	Right,
	Card,
	Picker
} from "native-base";

import { View, Text } from "react-native";

import styles from "./styles";

class BookSeat extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			fromValue: "Select",
			toValue: "Select",
			timeValue: "00:00"
		};
  }

  render() {
		const Places = this.props.places.map((item, index) => (
			<Picker.Item key={index} label={item} value={item} />
		));
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Book A Seat</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
        	<Card>
							<View style={{ padding: 15, borderBottomWidth: 0.5, borderColor: "#eee" }}>
								<Text>From</Text>
								<Picker
									note
									mode="dropdown"
									style={{ width: 240 }}
									selectedValue={this.state.fromValue}
									onValueChange={(e) => this.setState({ fromValue: e })}
								>
									<Picker.Item label="Select" value="Select" />
									{Places}
								</Picker>
	            </View>
			    		<View style={{ padding: 15, borderBottomWidth: 0.5, borderColor: "#eee" }}>
								<Text>To</Text>
								<Picker
									note
									mode="dropdown"
									style={{ width: 240 }}
									selectedValue={this.state.toValue}
									onValueChange={(e) => this.setState({ toValue: e })}
								>
									<Picker.Item label="Select" value="Select" />
									{Places}
								</Picker>
	            </View>
							<View style={{ padding: 15 }}>
								<Text>Date Time</Text>
								<Picker
									note
									mode="dropdown"
									style={{ width: 160 }}
									selectedValue={this.state.timeValue}
									onValueChange={(e) => this.setState({ timeValue: e })}
								>
									<Picker.Item label="00:00" value="00:00" />
									<Picker.Item label="01:00" value="01:00" />
									<Picker.Item label="02:00" value="02:00" />
									<Picker.Item label="03:00" value="03:00" />
									<Picker.Item label="04:00" value="04:00" />
									<Picker.Item label="05:00" value="05:00" />
									<Picker.Item label="06:00" value="06:00" />
									<Picker.Item label="07:00" value="07:00" />
									<Picker.Item label="08:00" value="08:00" />
									<Picker.Item label="09:00" value="09:00" />
									<Picker.Item label="10:00" value="10:00" />
									<Picker.Item label="11:00" value="11:00" />
									<Picker.Item label="12:00" value="12:00" />
									<Picker.Item label="13:00" value="13:00" />
									<Picker.Item label="14:00" value="14:00" />
									<Picker.Item label="15:00" value="15:00" />
									<Picker.Item label="16:00" value="16:00" />
									<Picker.Item label="17:00" value="17:00" />
									<Picker.Item label="18:00" value="18:00" />
									<Picker.Item label="19:00" value="19:00" />
									<Picker.Item label="20:00" value="20:00" />
									<Picker.Item label="21:00" value="21:00" />
									<Picker.Item label="22:00" value="22:00" />
									<Picker.Item label="23:00" value="23:00" />
								</Picker>
							</View>
							<Button
								full
								onPress={() => this.props.onFind(this.state.fromValue, this.state.toValue, this.state.timeValue)}
							>
		             <Text style={{ color:"white", fontWeight: "bold" }}>Go!</Text>
							</Button>
	          </Card>
        </Content>
    </Container>
      
    );
  }
}

export default BookSeat;