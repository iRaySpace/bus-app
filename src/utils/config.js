// Set either as "debug" / "production"
const ENVIRONMENT = "debug";

const DEBUG_URL = "192.168.42.121:8002";
const PRODUCTION_URL = "buszer.site";

export default {
  get siteUrl() {
    return ENVIRONMENT === "production" ? PRODUCTION_URL : DEBUG_URL;
  }
};
