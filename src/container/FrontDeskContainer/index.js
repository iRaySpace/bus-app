import * as React from "react";
import { observer, inject } from "mobx-react/native";

import FrontDesk from "../../stories/screens/FrontDesk";

@inject("mainStore")
@observer
export default class FrontDeskContainer extends React.Component {

  render() {
      return (
        <FrontDesk
          mainStore={this.props.mainStore}
          navigation={this.props.navigation}
        />
      );
  }

}
