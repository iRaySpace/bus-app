import * as React from "react";

import TrackBus from "../../stories/screens/TrackBus";

export default class TrackBusContainer extends React.Component {
  render() {
    return (
      <TrackBus
        navigation={this.props.navigation}
      />
    );
  }
}
