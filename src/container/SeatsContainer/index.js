import * as React from "react";
import { Toast } from "native-base";

import Seats from "../../stories/screens/Seats";
import { observer, inject } from "mobx-react/native";

@inject("seatForm")
@observer
export default class SeatsContainer extends React.Component {
  reserve(seat) {
    // '''Reserve for a seat'''
    if (this.seatReserved(seat)) {
      this.props.seatForm.unreserve(seat);
      Toast.show({
        text: `Unreserved ${seat}`,
        buttonText: "Okay"
      });
      return;
    }

    if (this.reservationMaxed()) {
      Toast.show({
        text: "Reservation maxed out",
        buttonText: "Okay"
      });
      return;
    }

    if (this.seatTaken(seat)) {
      Toast.show({
        text: `Seat ${seat} already taken`,
        buttonText: "Okay"
      });
      return;
    }

    const { seatForm } = this.props;
    
    // Reserved the seat
    seatForm.reserve(seat);
    
    // notify
    Toast.show({
      text: `Reserved for the Seat ${seat}`,
      buttonText: "Okay"
    });

  }

  seatReserved(seat) {
    // '''Check if user reserved.'''
    const { seatForm } = this.props;
    return (seatForm.reserved.indexOf(seat) > -1) ? true : false;
  }

  seatTaken(seat) {
    // '''Check if seat is taken (other client)
    const { seatForm } = this.props;
    return (seatForm.taken.indexOf(seat) > -1) ? true : false;
  }

  reservationMaxed() {
    // '''Check pax
    const { seatForm } = this.props;
    return (seatForm.reserved.length === seatForm.maxReservation) ? true : false;
  }

  render() {
    return (
      <Seats
        navigation={this.props.navigation}
        reserved={this.props.seatForm.reserved.slice()}
        onReserve={(seat) => this.reserve(seat)}
        isSeatTaken={(seat) => this.seatTaken(seat)}
        isSeatReserved={(seat) => this.seatReserved(seat)}
      />
    );
  }
}
