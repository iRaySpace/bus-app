import * as React from "react";
import { View  } from "react-native";

import BusSchedule from "../../stories/screens/BusSchedule";

export interface Props {
  navigation: any,
}

export interface State {}

export default class BusScheduleContainer extends React.Component{
  render(){
      return <BusSchedule navigation={this.props.navigation} />;
  }
}
