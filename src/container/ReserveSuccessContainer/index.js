import * as React from "react";

import ReserveSuccess from "../../stories/screens/ReserveSuccess";

export default class ReserveSuccessContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ReserveSuccess
        navigation={this.props.navigation}
      />
    );
  }
}
