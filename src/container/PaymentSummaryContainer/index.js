import * as React from "react";
import { inject, observer } from "mobx-react/native";

import PaymentSummary from "../../stories/screens/PaymentSummary";

@inject("mainStore")
@observer
export default class PaymentSummaryContainer extends React.Component {
  render() {
    return (
      <PaymentSummary
        navigation={this.props.navigation}
        code={this.props.mainStore.qrCode}
      />
    );
  }
}