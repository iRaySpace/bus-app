import * as React from "react";
import { Form, Item, Input, Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import Registration from "../../stories/screens/Registration";
import Config from "../../utils/config";

@inject("registrationForm")
@observer
export default class RegistrationContainer extends React.Component {
  
  registration() {
    // ''' Validates, and register for the user '''
    const form = this.props.registrationForm;

    form.validate();

    // Register the user
    if (form.isValid) {
      this.register_user();
    } else {
      Toast.show({
        text: "Your password and confirm password don't match",
        duration: 2000,
        position: "top",
        textStyle: { textAlign: "center" },
      });
    }

  }

  register_user() {
    // ''' Registers for the user by calling the API '''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.register_user`;

    // Get the form data
    const form = this.props.registrationForm;

    const new_user = {
      "email": form.email,
      "first_name": form.firstName,
      "last_name": form.lastName,
      "password": form.password
    };

    // Request data
    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(new_user)
    }).then(res => res.json())
      .then(res => {

        // Get the message from the result
        const { message } = res;

        // Reset the form
        form.clear();

        // Good boy!
        if (message.success) {
          Toast.show({
            text: "You have successfully registered!",
            duration: 2000,
            position: "bottom",
            textStyle: { textAlign: "center" }
          });
          this.props.navigation.navigate("Login");
        } else {
          Toast.show({
            text: "Something is wrong.",
            duration: 2000,
            position: "top",
            textStyle: { textAlign: "center" },
          });
        }

      })
      .catch(err => {
        console.error(err);
      });
    
  }

  render() {
    const form = this.props.registrationForm;
    const Fields = (
      <Form>
        <Item>
          <Input
            placeholder="First Name"
            value={form.firstName}
            onChangeText={e => form.firstNameOnChange(e)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Last Name"
            value={form.lastName}
            onChangeText={e => form.lastNameOnChange(e)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Email Address"
            value={form.email}
            onChangeText={e => form.emailOnChange(e)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Password"
            value={form.password}
            secureTextEntry={true}
            onChangeText={e => form.passwordOnChange(e)}
          />
        </Item>
        <Item>
          <Input
            placeholder="Confirm Password"
            value={form.confirmPassword}
            secureTextEntry={true}
            onChangeText={e => form.confirmPasswordOnChange(e)}
          />
        </Item>
      </Form>
    );
    return <Registration navigation={this.props.navigation} registrationForm={Fields} onRegistration={() => this.registration()}  />;
  }

}
