import * as React from "react";
import { observer, inject } from "mobx-react/native";

import AvailableBus from "../../stories/screens/AvailableBus";

@inject("busStore")
@observer
export default class AvailableBusContainer extends React.Component {

  select(i) {
    this.props.busStore.selectBus(i);
    this.props.navigation.navigate("PaymentDetails");
  }

  render() {

    let buses = [];

    if (this.props.busStore.buses) {
      buses = this.props.busStore.buses.slice();
    }

    return (
      <AvailableBus
        buses={buses}
        navigation={this.props.navigation}
        onSelectBus={(i) => this.select(i)}
      />
    );
  }
}
