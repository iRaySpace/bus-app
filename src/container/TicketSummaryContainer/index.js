import * as React from "react";
import { Alert } from "react-native";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import TicketSummary from "../../stories/screens/TicketSummary";
import Config from "../../utils/config";

@inject("ticketForm", "seatForm")
@observer
export default class TicketSummaryContainer extends React.Component {

  ticket() {
    // '''Generate the ticket'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_ticket`;

    const data = {
      "reference_payment": this.props.ticketForm.name,
      "bus_route": this.props.ticketForm.busRoute,
      "seats": this.props.seatForm.reserved.slice()
    };

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        
        const { message } = res;

        if (message.success) {
          Alert.alert("Reserve Seats", "Would you like to reserve these seats?", [
            {text: "Cancel", style: "cancel"},
            {text: "OK", onPress: () => this.reserve(message.tickets)},
          ]);
        }

      })
      .catch(err => {
        console.error(err);
      });

  }

  reserve(tickets) {
    // '''Reserve the tickets'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.reserve_ticket`;
    
    const data = {
      "reference_payment": this.props.ticketForm.name,
      "tickets": tickets
    };

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {

        const { message } = res;

        if (message.success) {

          Toast.show({
            text: "Tickets are reserved",
            duration: 2000,
            position: "top",
            textStyle: { textAlign: "center" }
          });

          // Clear ticket
          this.props.ticketForm.setScanned(false);
          
          this.props.navigation.navigate("ReserveSuccess");

        }

      })
      .catch(err => {
        console.error(err);
      });

  }

  render() {

    const { seatForm, ticketForm } = this.props;

    const payment = {
      customer: ticketForm.customer,
      method: ticketForm.method,
      datetime: ticketForm.datetime,
      busRoute: ticketForm.busRoute,
      busId: ticketForm.busId,
      from: ticketForm.from,
      to: ticketForm.to,
      fare: ticketForm.fare,
      pax: ticketForm.pax,
      paid: ticketForm.paid
    };

    return (
      <TicketSummary
        navigation={this.props.navigation}
        seats={seatForm.reserved.slice()}
        onTicket={() => this.ticket()}
        payment={payment}
      />
    );
  }
}
