import * as React from "react";

import TicketDetails from "../../stories/screens/TicketDetails";

export default class TicketDetailsContainer extends React.Component {
  render() {
    return <TicketDetails navigation={this.props.navigation} />;
  }
}