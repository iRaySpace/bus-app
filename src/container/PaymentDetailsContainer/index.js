import * as React from "react";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import PaymentDetails from "../../stories/screens/PaymentDetails";
import Config from "../../utils/config";

@inject("busStore", "mainStore")
@observer
export default class PaymentDetailsContainer extends React.Component {

  book(qty) {
    // '''Buszer Payment'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.book_bus`;
    
    // mainStore
    const { busStore, mainStore } = this.props;
    const { selectedBus } = busStore;
    
    const data = {
      "key": mainStore.key,
      "customer_name": `${mainStore.firstName} ${mainStore.lastName}`,
      "route_from": selectedBus.from,
      "route_to": selectedBus.to,
      "route_id": selectedBus.route_id,
      "bus_id": selectedBus.bus_id,
      "qty": parseInt(qty)
    };

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        
        const { message } = res;

        if (message.success) {

          const { data } = message;

          Toast.show({
            text: `${data.name} is created`,
            duration: 2000,
            position: "top",
            textStyle: { textAlign: "center" }
          });
          
          this.props.mainStore.setCode(data.name);
          this.props.navigation.navigate("PaymentSummary");

        }

      })
      .catch(err => {
        console.error(err);
      });

  }

  render() {
    return (
      <PaymentDetails
        navigation={this.props.navigation}
        onBook={(qty) => this.book(qty)}
        bus={this.props.busStore.selectedBus}
      />
    );
  }

}