import * as React from "react";
import { observer, inject } from "mobx-react/native";

import Announcements from "../../stories/screens/Announcements";
import Config from "../../utils/config";

@inject("newsStore")
@observer
export default class AnnouncementsContainer extends React.Component {

  componentWillMount() {
    // '''Fetching announcements from the website''' //

    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_announcements`;
    
    const { newsStore } = this.props;

    // For the UI
    newsStore.setFetching(true);

    // Getting data from the server
    fetch(url)
      .then(res => res.json())
      .then(res => {
        newsStore.fetchNews(res.message);
        newsStore.setFetching(false);
        newsStore.finishedLoading();
      })
      .catch(err => {
        console.error(err);
      });
    
  }

  render() {

    let news = [];

    if (this.props.newsStore.news) {
      news = this.props.newsStore.news.slice();
    }

    return (
      <Announcements
        navigation={this.props.navigation}
        news={news}
      />
    );
  }
}
