import * as React from "react";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import BookSeat from "../../stories/screens/BookSeat";
import Config from "../../utils/config";

@inject("mainStore", "busStore")
@observer
export default class BookSeatContainer extends React.Component {

  componentWillMount() {
    // '''Get the places'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_places`;

    fetch(url)
      .then(res => res.json())
      .then(res => {

        const { message } = res;

        if (message.success) {
          const { data } = message;
          this.props.mainStore.setPlaces(data);
        }

      });
  }

  find(from, to, time) {
    // '''Find bus based from from, to, time```

    // Check form
    if (from === to) {
      Toast.show({
        text: "Same place not allowed.",
        buttonText: "Okay",
        type: "warning"
      });
      return;
    }

    // Should select places
    if (from === "Select" || to === "Select") {
      Toast.show({
        text: "Please select the place.",
        buttonText: "Okay",
        type: "warning"
      });
      return;
    }

    const url = `http://${Config.siteUrl}/api/method/buszer.api.find_buses`;

    const data = {
      "from": from,
      "to": to,
      "time": time
    };

    const { busStore } = this.props;

    // For the UI
    busStore.setFetching(true);

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        busStore.fetchBuses(res.message);
        busStore.setFetching(false);
        busStore.finishedLoading();
        this.props.navigation.navigate("AvailableBus");
      })
      .catch(err => {
        console.error(err);
      });
      
  }

  render() {
      return (
        <BookSeat
          navigation={this.props.navigation}
          onFind={(from, to, time) => this.find(from, to, time)}
          places={this.props.mainStore.places.slice()}
        />
      );
  }

}
