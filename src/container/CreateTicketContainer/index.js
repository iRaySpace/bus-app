import * as React from "react";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import CreateTicket from "../../stories/screens/CreateTicket";
import Config from "../../utils/config";

@inject("ticketForm", "seatForm")
@observer
export default class CreateTicketContainer extends React.Component {

  read(e) {
    // '''Get the data from the server'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_payment_details`;

    // '''Name of the Buszer Payment'''
    const name = e.data;

    const data = {
      "name": name
    };

    const { ticketForm, seatForm } = this.props;

    // Fetch data from the server
    ticketForm.setFetching(true);

    // Set the scan
    ticketForm.setScanned(true);

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {

        const { message } = res;

        if (message.success) {
          
          const { data } = message;

          Toast.show({
            text: `${data.name} is found`,
            duration: 2000,
            position: "top",
            textStyle: { textAlign: "center" }
          });

          // Load the data
          ticketForm.fetchData(data);
          
          // Yeah, man!
          seatForm.setMaxReservation(data.pax);

          // Get taken seats
          this.taken(data.bus_route);

        }

        ticketForm.setFetching(false);

      });

  }

  taken(bus_route) {
    // '''Gets the seats taken'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_seats_taken`;

    const data = {
      "bus_route": bus_route
    };

    const { seatForm } = this.props;

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        
        const { message } = res;

        if (message.success) {

          // Ey!!!
          const { data } = message;

          // Set the taken seats for seats container
          seatForm.fetchTaken(data);

        }

      });

  }

  scanAgain() {
    // '''Scan again for the data'''
    this.props.ticketForm.setScanned(false);
  }

  render() {

    const { ticketForm } = this.props;

    const payment = {
      customer: ticketForm.customer,
      method: ticketForm.method,
      datetime: ticketForm.datetime,
      busRoute: ticketForm.busRoute,
      busId: ticketForm.busId,
      from: ticketForm.from,
      to: ticketForm.to,
      fare: ticketForm.fare,
      pax: ticketForm.pax,
      paid: ticketForm.paid
    };

    return (
      <CreateTicket
        scanned={this.props.ticketForm.scanned}
        navigation={this.props.navigation}
        onScan={() => this.scanAgain()}
        onRead={(e) => this.read(e)}
        payment={payment}
      />
    );
  }

}
