import * as React from "react";
import { Alert } from "react-native";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import Tickets from "../../stories/screens/Tickets";
import Config from "../../utils/config";

@inject("mainStore", "ticketStore")
@observer
export default class TicketsContainer extends React.Component {

  componentWillMount() {

    // '''Get the tickets
    const url = `http://${Config.siteUrl}/api/method/buszer.api.get_tickets`;

    const data = {
      "key": this.props.mainStore.key
    };

    // Fetching Tickets
    this.props.ticketStore.setFetching(true);

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        const { message } = res;

        if (message.success) {
          const { data } = message;
          
          this.props.ticketStore.setFetching(false);
          this.props.ticketStore.fetchTickets(data);
        }

      })
      .catch(err => console.error(err));
  }

  cancel(index) {
    // '''Cancel the ticket'''
    const url = `http://${Config.siteUrl}/api/method/buszer.api.cancel_ticket`;
    
    // Ticket
    const ticket = this.props.ticketStore.tickets[index];

    // Data - Ticket Number
    const data = {
      "ticket": ticket.ticket_number
    };

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(res => {
        const { message } = res;
        if (message.success) {
          Toast.show({
            text: "Cancelled Ticket",
            buttonText: "Okay"
          });
        }
      });

    // Remove from the array
    this.props.ticketStore.removeTicket(index);

  }

  render() {
    return (
      <Tickets
        navigation={this.props.navigation}
        onCancel={(index) => this.cancel(index)}
        tickets={this.props.ticketStore.tickets.slice()}
      />
    );
  }

}
