import * as React from "react";
import { Item, Input, Icon, Form, Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import Login from "../../stories/screens/Login";
import Config from "../../utils/config";

@inject("loginForm", "mainStore")
@observer
export default class LoginContainer extends React.Component {

	login() {
		this.props.loginForm.validateForm();
		if (this.props.loginForm.isValid) {
			this.authenticate_user();
		} else {
			Toast.show({
				text: "Enter Valid Email & password!",
				duration: 2000,
				position: "top",
				textStyle: { textAlign: "center" },
			});
		}
	}

	authenticate_user() {
		// ''' Authenticate the user by calling the API '''
		const url = `http://${Config.siteUrl}/api/method/buszer.api.authenticate_user`;

		// Get the form data
		const form = this.props.loginForm;

		const user = {
			"email": form.email,
			"password": form.password
		};

		fetch(url, {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(user)
		}).then(res => res.json())
			.then(res => {

				const { message } = res;
				
				form.clearStore();

				if (message.success) {
					
					// Set the data needed to the mainstore
					const { data } = message;
					
					// mainStore
					const main = this.props.mainStore;

					// Initialize the main store
					main.init(data.key, data.buszer_points, data.first_name, data.last_name);

					Toast.show({
						text: "Welcome to the Buszer!",
						duration: 2000,
						position: "top",
						textStyle: { textAlign: "center" }
					});

					this.props.navigation.navigate("Drawer");

				} else {
					Toast.show({
            text: "Something is wrong.",
            duration: 2000,
            position: "top",
            textStyle: { textAlign: "center" },
          });
				}

			})
			.catch(err => {
				console.error(err);
			});

	}

	register() {
		this.props.navigation.navigate("Registration");
	}

	render() {
		const form = this.props.loginForm;
		const Fields = (
			<Form>
				<Item error={form.emailError ? true : false}>
					<Icon active name="person" />
					<Input
						placeholder="Email"
						keyboardType="email-address"
						ref={c => (this.emailInput = c)}
						value={form.email}
						onBlur={() => form.validateEmail()}
						onChangeText={e => form.emailOnChange(e)}
					/>
				</Item>
				<Item error={form.passwordError ? true : false}>
					<Icon active name="unlock" />
					<Input
						placeholder="Password"
						ref={c => (this.pwdinput = c)}
						value={form.password}
						onBlur={() => form.validatePassword()}
						onChangeText={e => form.passwordOnChange(e)}
						secureTextEntry={true}
					/>
				</Item>
			</Form>
		);
		return <Login navigation={this.props.navigation} loginForm={Fields} onLogin={() => this.login()} onRegister={() => this.register()} />;
	}
}