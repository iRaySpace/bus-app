import { observable, action } from "mobx";

class TicketStore {

  @observable tickets = [];
  @observable isFetching = false;

  @action
  setFetching(fetching) {
    this.isFetching = fetching;
  }

  @action
  fetchTickets(tickets) {
    this.tickets = tickets;
  }

  @action
  removeTicket(index) {
    this.tickets.splice(index, 1);
  }

}

export default TicketStore;
