import { observable, action } from "mobx";

class BusStore {

  @observable buses = [];
  @observable isFetching = false;
  @observable loaded = false;
  @observable selectedBus = "";
  
  @action
  selectBus(bus) {
    this.selectedBus = this.buses[bus];
  }

  @action
  setFetching(fetching) {
    this.isFetching = fetch;
  }

  @action
  finishedLoading() {
    this.loaded = true;
  }

  @action
  fetchBuses(data) {
    this.buses = data;
  }

}

export default BusStore;