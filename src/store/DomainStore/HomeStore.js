import { observable, action } from "mobx";

class HomeStore {
  
  @observable firstName = "";
  @observable lastName = "";
  @observable balance = "";
  @observable key = "";
  @observable qrCode = "";
  @observable places = [];

  @action
  init(key, balance, firstName, lastName) {
    this.setKey(key);
    this.setBalance(balance);
    this.setFirstName(firstName);
    this.setLastName(lastName);
  }

  @action
  setFirstName(firstName) {
    this.firstName = firstName;
  }

  @action
  setLastName(lastName) {
    this.lastName = lastName;
  }

  @action
  setBalance(balance) {
    this.balance = balance;
  }

  @action
  setKey(key) {
    this.key = key;
  }

  @action
  setCode(code) {
    this.qrCode = code;
  }
  
  @action
  setPlaces(places) {
    this.places = places;
  }

}

export default HomeStore;
