import { observable, action } from "mobx";

class AnnouncementStore {
 
  @observable news = [];
  @observable isFetching = false;
  @observable loaded = false;

  @action
  setFetching(fetching) {
    this.isFetching = fetching;
  }

  @action 
  finishedLoading() {
    this.loaded = true;
  }

  @action
  fetchNews(data) {
    this.news = data;
  }

}

export default AnnouncementStore;
