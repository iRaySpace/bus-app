import { observable, action } from "mobx";

class SeatsViewStore {

  @observable reserved = [];
  @observable taken = [];
  @observable maxReservation = 0;

  @action
  fetchTaken(taken) {
    this.taken = taken;
  }

  @action
  reserve(seat) {
    this.reserved.push(seat);
  }

  @action
  unreserve(seat) {
    const index = this.reserved.indexOf(seat);
    this.reserved.splice(index, 1);
  }

  @action
  setMaxReservation(reservation) {
    this.maxReservation = reservation;
  }

}

export default SeatsViewStore;
