import { observable, action } from "mobx";

class CreateTicketViewStore {

  @observable name = "";
  @observable customer = "";
  @observable method = "";
  @observable datetime = "";
  @observable busRoute = "";
  @observable busId = "";
  @observable from = "";
  @observable to = "";
  @observable fare = "";
  @observable pax = "";
  @observable paid = "";

  @observable isFetching = false;
  @observable scanned = false;

  @action
  setFetching(fetching) {
    this.isFetching = fetching;
  }

  @action
  setScanned(scanned) {
    this.scanned = scanned;
  }
  
  @action
  fetchData(data) {
    this.name = data.name;
    this.customer = data.customer;
    this.method = data.method;
    this.datetime = data.datetime;
    this.busRoute = data.bus_route;
    this.busId = data.bus_id;
    this.from = data.route_from;
    this.to = data.route_to;
    this.fare = data.fare;
    this.pax = data.pax;
    this.paid = data.paid;
  }

}

export default CreateTicketViewStore;
