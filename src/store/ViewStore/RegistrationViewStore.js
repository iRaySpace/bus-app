import { observable, action } from "mobx";

class RegistrationStore {

  @observable email = "";
  @observable firstName = "";
  @observable lastName = "";
  @observable password = "";
  @observable confirmPassword = "";
  @observable isValid = false;

  @action
  clear() {
    email = "";
    firstName = "";
    lastName = "";
    password = "";
    confirmPassword = "";
    isValid = false;
  }

  @action
  validate() {
    if (this.password === this.confirmPassword) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  @action
  emailOnChange(email) {
    this.email = email;
  }

  @action
  firstNameOnChange(firstName) {
    this.firstName = firstName;
  }

  @action
  lastNameOnChange(lastName) {
    this.lastName = lastName;
  }

  @action
  passwordOnChange(password) {
    this.password = password;
  }

  @action
  confirmPasswordOnChange(password) {
    this.confirmPassword = password;
  }

};

export default RegistrationStore;