
## IE Collaboration - Bus Ticketing Application

### A bus ticketing application project

## Get Started

### 1. System Requirements

* Globally installed [node](https://nodejs.org/en/)

* Globally installed [react-native CLI](https://facebook.github.io/react-native/docs/getting-started.html)


### 2. Installation

On the command prompt run the following commands

```sh
$ git clone git@gitlab.com:iRaySpace/bus-app.git

$ cd bus-app/

$ npm install
  or
  yarn
```

### Run on iOS

  * Opt #1:
		*	Run `npm start` in your terminal
		*	Scan the QR code in your Expo app
	*	Opt #2:
		*	Run `npm run ios` in your terminal

### Run on Android

  * Opt #1:
		*	Run `npm start` in your terminal
		*	Scan the QR code in your Expo app
	*	Opt #2:
		*	Run `npm run android` in your terminal


For apps with more advance designs, please visit **[NativeBase Market](https://market.nativebase.io/)**.
